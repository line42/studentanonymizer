﻿using NDesk.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;

namespace StudentAnonymizer
{
    class Program
    {

        private static bool reverse = false;
        private static bool recursive = false;
        private static bool verbose = false;
        private static bool extractNames = false;

        private static readonly int LastNameColumn = 1;
        private static readonly int FirstNameColumn = 2;
        private static readonly int MatriculationNumberColumn = 4;

        private static readonly String[] ForwardSearchMask = new String[] { "*.c", "*.h", "*.cpp" };
        private static readonly String[] ReverseSearchMask = new String[] { "*.c", "*.h", "*.cpp", "*.md" };

        private static readonly Regex UmlautPattern = new Regex(@"([äÄöÖüÜß])");

        private static OptionSet options = new OptionSet()
        {
            {"d|decrypt","Change to decrypt mode",v => reverse = true },
            {"r|recursive", "Change to recursive mode", v => recursive = true },
            {"v|verbose", "Increase verbosity", v => verbose = true},
            {"e|extract", "Extract names and matriculation number", v => extractNames = true }
        };

        static void Main(string[] args)
        {
            List<String> extra = null;
            try
            {
                extra = options.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Error.WriteLine("Invalid Arguments");
                Environment.Exit(1);
            }
            if (recursive && extractNames)
            {
                Console.Error.WriteLine("Cannot combine recursive mode and extract names");
                Environment.Exit(1);
            }

            IEnumerable<IEnumerable<String>> csv = null;
            try
            {
                csv = ReadCSV(extra[0]);
            }
            catch (FileNotFoundException e)
            {
                Console.Error.WriteLine("Namefile not found");
                Environment.Exit(1);
            }
            var names = reverse ? Rot13(ExtractNames(csv)) : ExtractNames(csv);
            var patterns = new Dictionary<Regex, Tuple<String, String, String, String>>();
            foreach (var name in names)
            {
                patterns.Add(GeneratePattern(name), name);
            }
            var files = EnumerateFiles(extra[1], recursive, reverse);

            var foundNames = new HashSet<Tuple<String, String, String, String>>();

            int replacedOccurences = 0;

            foreach (var file in files)
            {
                var content = "";
                if (verbose)
                    Console.WriteLine($"Searching in {file.Name}");
                using (StreamReader reader = file.OpenText())
                {
                    content = reader.ReadToEnd();
                }
                var changedContent = "";
                var lines = content.Split('\n');
                foreach (var line in lines)
                {
                    var tmpLine = line;
                    foreach (var pattern in patterns)
                    {
                        var match = pattern.Key.Match(line);
                        if (match.Success)
                        {
                            if (!foundNames.Contains(pattern.Value))
                            {
                                foundNames.Add(pattern.Value);
                            }
                            replacedOccurences++;
                            tmpLine = tmpLine.Replace(match.Groups[1].Value, Rot13(match.Groups[1].Value));
                        }
                    }
                    changedContent += tmpLine;
                    changedContent += '\n';
                }
                using (StreamWriter writer = new StreamWriter(file.OpenWrite()))
                {
                    writer.Write(changedContent);
                }
                if (extractNames)
                {
                    try
                    {
                        using (StreamWriter writer = new StreamWriter(new FileInfo(Path.Combine(extra[1], "members.txt")).OpenWrite()))
                        {
                            foreach (var name in foundNames)
                            {
                                writer.WriteLine($"{Rot13(name.Item1)} {Rot13(name.Item3)} ({name.Item4})");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("Could not write to members.txt");
                        Environment.Exit(1);
                    }
                }
            }
            if (verbose)
                if (reverse)
                    Console.WriteLine($"\"Decrypted\" {replacedOccurences} names");
                else
                    Console.WriteLine($"\"Encrypted\" {replacedOccurences} names");
        }
        static IEnumerable<FileInfo> EnumerateFiles(String path, bool recursive, bool reverse)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            IEnumerable<FileInfo> filteredFiles = null;
            var mask = reverse ? ReverseSearchMask : ForwardSearchMask;
            if (recursive)
            {
                filteredFiles = mask.SelectMany(obj =>
                {
                    return directory.EnumerateFiles(obj, SearchOption.AllDirectories);
                });
            }
            else
            {
                filteredFiles = mask.SelectMany(obj =>
                {
                    return directory.EnumerateFiles(obj, SearchOption.TopDirectoryOnly);
                });
            }
            return filteredFiles;
        }
        static List<Tuple<String, String, String, String>> ExtractNames(IEnumerable<IEnumerable<String>> csv)
        {
            var names = new List<Tuple<String, String, String, String>>();
            foreach (var line in csv.Where(l => l.Count() > 1))
            {
                var asList = line.ToList();
                var first = asList[FirstNameColumn];
                var last = asList[LastNameColumn];
                var matriculationNumber = asList[MatriculationNumberColumn];
                var middle = "";

                //Has middle name?
                if (first.Contains(" "))
                {
                    var parts = first.Split(' ');
                    first = parts[0];
                    middle = parts[1];
                }
                var name = Tuple.Create(first, middle, last, matriculationNumber);
                names.Add(name);
                var variant = ReplaceVariant(name);
                if (variant != null)
                    names.Add(variant);
            }
            return names;
        }
        static Tuple<String, String, String, String> ReplaceVariant(Tuple<String, String, String, String> name)
        {
            String f, m, n;
            bool isDifferent = false;
            Tuple<String, String, String, String> variant = null;
            var fMatch = UmlautPattern.Match(name.Item1);
            if (fMatch.Success)
            {
                var umlaut = fMatch.Groups[1].Value;
                f = name.Item1.Replace(umlaut, GetReplacement(umlaut));
                isDifferent = true;
            }
            else
            {
                f = name.Item1;
            }
            var mMatch = UmlautPattern.Match(name.Item2);
            if (mMatch.Success)
            {
                var umlaut = mMatch.Groups[1].Value;
                m = name.Item2.Replace(umlaut, GetReplacement(umlaut));
                isDifferent = true;
            }
            else
            {
                m = name.Item2;
            }
            var nMatch = UmlautPattern.Match(name.Item3);
            if (nMatch.Success)
            {
                var umlaut = nMatch.Groups[1].Value;
                n = name.Item3.Replace(umlaut, GetReplacement(umlaut));
                isDifferent = true;
            }
            else
            {
                n = name.Item3;
            }
            if (isDifferent)
                variant = Tuple.Create(f, m, n, name.Item4);
            return variant;
        }
        static String GetReplacement(String umlaut)
        {
            switch (umlaut)
            {
                case "ö":
                    return "oe";
                case "ä":
                    return "ae";
                case "ü":
                    return "ue";
                case "ß":
                    return "ss";
                case "Ö":
                    return "Oe";
                case "Ä":
                    return "Ae";
                case "Ü":
                    return "Ue";
                default:
                    return umlaut;
            }
        }
        static Regex GeneratePattern(Tuple<String, String> nameTuple)
        {
            return new Regex(String.Format(@"^\/\/.*({0}\s{{1,5}}{1}|{1}\s{{1,5}}{0})", nameTuple.Item1, nameTuple.Item2), RegexOptions.IgnoreCase);
        }
        static Regex GeneratePattern(Tuple<String, String, String, String> nameTuple)
        {
            //Check for middle name
            if (String.IsNullOrEmpty(nameTuple.Item2))
                return GeneratePattern(Tuple.Create(nameTuple.Item1, nameTuple.Item3));
            return new Regex(String.Format(@"^\/\/.*({0}\s{{1,5}}{1}\s{{1,5}}{2}|{2}\s{{1,5}}{1}\s{{1,5}}{0}|{2}\s{{1,5}}{1}\s{{1,5}}{0}|{2}\s{{1,5}}{0}\s{{1,5}}{1}|{0}\s{{1,5}}{2}|{2}\s{{1,5}}{0})", nameTuple.Item1, nameTuple.Item2, nameTuple.Item3), RegexOptions.IgnoreCase);
        }
        static IEnumerable<IEnumerable<String>> ReadCSV(String path)
        {
            var contents = File.ReadAllText(path).Split('\n');
            var csv = from line in contents
                      select line.Trim().Split(',').ToArray();
            //Skip one to ignore header
            return csv.Skip(1);
        }
        static List<Tuple<String, String, String, String>> Rot13(List<Tuple<String, String, String, String>> names)
        {
            List<Tuple<String, String, String, String>> rotList = new List<Tuple<String, String, String, String>>();
            foreach (var t in names)
            {
                rotList.Add(Tuple.Create(Rot13(t.Item1), Rot13(t.Item2), Rot13(t.Item3), t.Item4));
            }
            return rotList;
        }
        static string Rot13(string input)
        {
            StringBuilder result = new StringBuilder();
            Regex regex = new Regex("[A-Za-z]");

            foreach (char c in input)
            {
                if (regex.IsMatch(c.ToString()))
                {
                    int charCode = ((c & 223) - 52) % 26 + (c & 32) + 65;
                    result.Append((char)charCode);
                }
                else
                {
                    result.Append(c);
                }
            }

            return result.ToString();
        }
    }
}
